package ar.fiuba.tdd.tp0;
import java.util.*;
import java.io.*;

public class Calculator {
    private float result_;
    private HashMap<String, Operation> map_operators_;
    
    public Calculator(){
    	result_ = 0;
    	map_operators_ = new HashMap<String, Operation>();
		map_operators_.put("+",new Add());
		map_operators_.put("++",new Add());
		map_operators_.put("-",new Substraction());
		map_operators_.put("--",new Substraction());
		map_operators_.put("*",new Multiplication());
		map_operators_.put("**",new Multiplication());
		map_operators_.put("/",new Division());
		map_operators_.put("//",new Division());
		map_operators_.put("MOD",new Mod());
		map_operators_.put("MODMOD",new Mod());
		
    }
	
    /**
     * Realiza la operacion recibida en operator, con los operandos op1 y op2
     */
    public void do_operation(float op1,float op2, String operator){
		Operation operation = map_operators_.get(operator);
		while (operation == null){
			throw new IllegalArgumentException();
		}
		result_ = operation.do_operation(op1,op2);
	}

	/**
	 * Ejecuta las operaciones con los operandos que estan en la cola y el operando 
	 * en la variable operator
	 */
	public void run(Queue<Float> queue_numbers, String operator){
		while (queue_numbers.size() == 1 && operator.length() != 2){
			throw new IllegalArgumentException();//nothing (Ej 5 +)
    	}
		while (queue_numbers.size() == 1 && operator.length() == 2 && map_operators_.get(operator) != null){
    		result_ = queue_numbers.poll();//nothing (Ej 5 ++)
    	}
		while (queue_numbers.size() > 1){
    		    //Obtengo operandos
    			float op1 = queue_numbers.poll();
        	    float op2 = queue_numbers.poll();
        	    do_operation(op1,op2,operator);
        	    float parcial_result = result_;
        	    while (queue_numbers.size() >= 1){
        	    	op2 = queue_numbers.poll();
        	    	do_operation(parcial_result,op2,operator);
        	    	parcial_result = result_;
        	    }        	    
    	}
		
		queue_numbers.add(result_);
    }

	    
}
