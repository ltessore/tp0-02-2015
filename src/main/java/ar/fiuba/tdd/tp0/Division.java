package ar.fiuba.tdd.tp0;

public class Division implements Operation {
	
	public Division(){
	}
	
	public float do_operation(float op_1, float op_2){
		return op_1 / op_2;
	}
	

}
