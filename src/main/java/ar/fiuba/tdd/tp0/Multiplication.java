package ar.fiuba.tdd.tp0;

/**
 * Representa a la operacion multiplicacion
 */
public class Multiplication implements Operation {
		
	public Multiplication(){
	}
	
	public float do_operation(float op_1, float op_2){
		return op_1 * op_2;
	}
	


}
