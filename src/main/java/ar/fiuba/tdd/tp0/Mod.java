package ar.fiuba.tdd.tp0;


public class Mod implements Operation {
	
	public Mod(){
	}
	
	public float do_operation(float op_1, float op_2){
		return op_1 % op_2;
	}
	
}
