package ar.fiuba.tdd.tp0;

/**
 * Interface que representa a las operaciones aritmeticas
 */
public interface Operation {

        /**
         * Realiza una operacion aritmetica
         */
	public float do_operation(float op_1, float op_2);
}
