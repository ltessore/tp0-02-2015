package ar.fiuba.tdd.tp0;
import java.util.*;

/**
 * Separa los operandos del operador
 */
public class Distributor {
		
	public void set_entry(String entry){
		entry_ = entry;
	}
	
	public void set_queue_numbers(Queue<Float> queue_numbers){
		queue_numbers_ = queue_numbers;
	}
    
	public void set_operator(String op){
		operator_ = op;
	}
	
	boolean is_operator(){
		return entry_.equals(operator_);
	}
	
	boolean is_number(){
		boolean is_number = true;
		for (Integer i = 0; i< entry_.length();i++){
			is_number = Character.isDigit(entry_.charAt(i));
		}
		return is_number;
	}
	
	//Si es un operando lo agrega a la pila de operandos, sino asigna el operador
	public void run(){
		if (is_number()){			
			queue_numbers_.add(Float.parseFloat(entry_));
		}
		else{
			operator_ = entry_;
		}
	}
	
	
	private String entry_; //entrada actual
	private Queue<Float> queue_numbers_; //pila de operandos
	public String operator_; //operador actual
	
	
}
